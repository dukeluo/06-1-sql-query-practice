/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
SELECT MIN(ordercount.count) AS minOrderItemCount, MAX(ordercount.count) AS maxOrderItemCount, FLOOR(AVG(ordercount.count)) AS avgOrderItemCount
FROM (
    SELECT COUNT(orderNumber) AS count
	FROM orderdetails
	GROUP BY orderNumber)
    AS ordercount;