/*
 * 请告诉我所有的订单（`order`）中每一种 `status` 的订单的总金额到底是多少。注意是总金额哦。输出
 * 应当包含如下的信息：
 *
 * +─────────+─────────────+
 * | status  | totalPrice  |
 * +─────────+─────────────+
 *
 * 输出应当根据 `status` 进行排序。
 */
SELECT o.status, SUM(d.priceEach * d.quantityOrdered) AS totalPrice
FROM orders AS o,
     orderdetails AS d
WHERE o.orderNumber = d.orderNumber
GROUP BY o.status
ORDER BY o.status;

-- SELECT o.status, SUM(d.priceEach * d.quantityOrdered) AS totalPrice
-- FROM orders AS o
-- LEFT JOIN orderdetails AS d
-- ON o.orderNumber = d.orderNumber
-- GROUP BY o.status
-- ORDER BY o.status;
